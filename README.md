# Software Studio 2018 Spring Assignment 01 Web Canvas

## Web Canvas
<img src="example01.gif" width="700px" height="500px"></img>

## Todo
1. **Fork the repo ,remove fork relationship and change project visibility to public.**
2. Create your own web page with HTML5 canvas element where we can draw somethings.
3. Beautify appearance (CSS).
4. Design user interaction widgets and control tools for custom setting or editing (JavaScript).
5. **Commit to "your" project repository and deploy to Gitlab page.**
6. **Describing the functions of your canvas in REABME.md**

## Scoring (Check detailed requirments via iLMS)

|                       **Item**                   | **Score** |
|:--------------------------------------------:|:-----:|
|               Basic components               |  60%  |
|                 Advance tools                |  35%  |
|            Appearance (subjective)           |   5%  |
| Other useful widgets (**describe on README.md**) | 1~10% |

## Reminder
* Do not make any change to our root project repository.
* Deploy your web page to Gitlab page, and ensure it works correctly.
    * **Your main page should be named as ```index.html```**
    * **URL should be : https://105062224.gitlab.io/AS_01_WebCanvas**
* You should also upload all source code to iLMS.
    * .html or .htm, .css, .js, etc.
    * source files
* **Deadline: 2018/04/05 23:59 (commit time)**
    * Delay will get 0 point (no reason)
    * Copy will get 0 point
    * "屍體" and 404 is not allowed


1. draw & nodraw：控制mousedown跟mouseup時要做什麼
2. getMousePos：獲得滑鼠在canvas上的座標
3. click系列的function：在click工具時給予對應的type讓draw&nodraw進行對應的行為
4. putPoint：進行畫線的功能
5. CC(change color)：改變顏色
6. CB(change brush)：改變筆的粗細，有5、10、20、30，四種筆的半徑可以選擇
7. DS、DS2(draw square)：畫方形的function
8. DC、DC2(draw circle)：畫圓圈的function
9. DT、DT2(draw triangle)：畫三角形的function
(必須將滑鼠按下+放開後才會將圖片印出來)
10. CFS(change font size)：用來改變字體與字形
11. Push：將canvas的圖存入陣列裡
12. undo：回上一步
13. redo：回上一步
14. paste：上傳圖片
15. downloadCanvas：下載圖片

   
