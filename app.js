//brush

var canvas = document.getElementById('canvas');
var context = canvas.getContext('2d');

canvas.width = window.innerWidth;
canvas.height = window.innerHeight;
context.fillStyle='white';
context.fillRect(0,0,canvas.width,canvas.height);

var radius=10;

var dragging = false;

var type=0;

var start_x,start_y,end_x,end_y; //draw square & triangle

context.lineWidth=radius*2;


function getMousePos(canvas,e){
    var rect = canvas.getBoundingClientRect();
    return{
        x: (e.clientX - rect.left)*canvas.width/(rect.right-rect.left)+10,
        y: (e.clientY - rect.top)*canvas.height/(rect.bottom-rect.top)+55
    };
}

var putPoint = function(e){
    var pos = getMousePos(canvas,e);
    if(dragging){
        context.lineTo(pos.x,pos.y);
        context.stroke();
        context.beginPath();
        context.arc(pos.x,pos.y, radius, 0, Math.PI*2);
        context.fill();
        context.beginPath();
        context.moveTo(pos.x,pos.y);
    }
}

var draw = function(e){
    if(type==0||type==1){
        dragging = true;
        putPoint(e);
    }
    else if(type==2)
    {
        CFS(e);
    }
    else if(type==3)
    {
        DS(e);
    }
    else if(type==4)
    {
        DC(e);
    }
    else if(type==5)
    {
        DT(e);
    }
}

var nodraw = function(e){
    if(type==3)
    {
        DS2(e);
    }
    else if(type==4)
    {
        DC2(e);
    }
    else if(type==5)
    {
        DT2(e);
    }
    else {
        dragging = false;
        context.beginPath();
        Push();
    }
}

canvas.addEventListener('mousedown', draw);
canvas.addEventListener('mousemove' , putPoint);
canvas.addEventListener('mouseup', nodraw);

// change color

var change_color=document.getElementById('change_color');

change_color.addEventListener('change', CC);

function CC(){
    if(type==1) return ;
    for(var i=0;i<change_color.length;i++)
    {
        if(change_color[i].checked)
        {
            getColor(change_color[i].value);
            break;
        }
    }
}


function getColor(value)
{
    if(value=="black")
    {
        context.strokeStyle="black";
        context.fillStyle="black";   
    }
    else if(value=="red")
    {
        context.strokeStyle="red";
        context.fillStyle="red";   
    }
    else if(value=="orange")
    {
        context.strokeStyle="orange";
        context.fillStyle="orange";   
    }
    else if(value=="yellow")
    {
        context.strokeStyle="yellow";
        context.fillStyle="yellow";   
    }
    else if(value=="green")
    {
        context.strokeStyle="green";
        context.fillStyle="green";   
    }
    else if(value=="blue")
    {
        context.strokeStyle="blue";
        context.fillStyle="blue";   
    }
    else if(value=="purple")
    {
        context.strokeStyle="purple";
        context.fillStyle="purple";   
    }
}


// change brush

var brush_5 = document.getElementById('brush_5'),
    brush_10 = document.getElementById('brush_10'),
    brush_20 = document.getElementById('brush_20'),
    brush_30 = document.getElementById('brush_30');

function CB(num){
    if(num==5){
        radius=5;
        brush_5.style.color="red";
        brush_5.style.fontWeight="bold";
        brush_10.style.color="black";
        brush_10.style.fontWeight="normal";
        brush_20.style.color="black";
        brush_20.style.fontWeight="normal";
        brush_30.style.color="black";
        brush_30.style.fontWeight="normal";
    }
    else if(num==10){
        radius=10;
        brush_5.style.color="black";
        brush_5.style.fontWeight="normal";
        brush_10.style.color="red";
        brush_10.style.fontWeight="bold";
        brush_20.style.color="black";
        brush_20.style.fontWeight="normal";
        brush_30.style.color="black";
        brush_30.style.fontWeight="normal";
    }
    else if(num==20){
        radius=20;
        brush_5.style.color="black";
        brush_5.style.fontWeight="normal";
        brush_10.style.color="black";
        brush_10.style.fontWeight="normal";
        brush_20.style.color="red";
        brush_20.style.fontWeight="bold";
        brush_30.style.color="black";
        brush_30.style.fontWeight="normal";
    }
    else if(num==30){
        radius=30;
        brush_5.style.color="black";
        brush_5.style.fontWeight="normal";
        brush_10.style.color="black";
        brush_10.style.fontWeight="normal";
        brush_20.style.color="black";
        brush_20.style.fontWeight="normal";
        brush_30.style.color="red";
        brush_30.style.fontWeight="bold";
    }
    context.lineWidth=radius*2;
}

//draw square & triangle & circle
function DS(e)
{
    var pos = getMousePos(canvas,e);
    start_x=pos.x;
    start_y=pos.y;
}

function DS2(e)
{
    var pos = getMousePos(canvas,e);
    end_x=pos.x;
    end_y=pos.y;
    context.fillRect(start_x, start_y, end_x-start_x, end_y-start_y);
}
function DC(e)
{
    var pos = getMousePos(canvas,e);
    start_x=pos.x;
    start_y=pos.y;
}

function DC2(e)
{
    var pos = getMousePos(canvas,e);
    end_x=pos.x;
    end_y=pos.y;
    context.beginPath();
    context.arc((end_x+start_x)/2,(end_y+start_y)/2, Math.sqrt((end_x-start_x)*(end_x-start_x)+(end_y-start_y)*(end_y-start_y))/2, 0, Math.PI*2);
    context.fill();
}

function DT(e)
{
    var pos = getMousePos(canvas,e);
    start_x=pos.x;
    start_y=pos.y;
}

function DT2(e)
{
    var pos = getMousePos(canvas,e);
    end_x=pos.x;
    end_y=pos.y;
    context.beginPath();
    context.moveTo(start_x,start_y);
    context.lineTo(end_x,end_y);
    context.lineTo(start_x-(end_x-start_x),end_y);
    context.lineTo(start_x,start_y);
    context.fill();
}


// tool
var clear = document.getElementById('clear');
var pen = document.getElementById('pen');
var eraser = document.getElementById('eraser');
var text = document.getElementById('text');
var square = document.getElementById('square');
var circle = document.getElementById('circle');
var triangle = document.getElementById('triangle');

pen.addEventListener('click', click_pen);
eraser.addEventListener('click', click_eraser);
text.addEventListener('click', click_font);
clear.addEventListener('click', click_clear);
square.addEventListener('click', click_square);
circle.addEventListener('click', click_circle);
triangle.addEventListener('click', click_triangle);

function click_eraser(){
    type=1;
    context.strokeStyle="white";
    context.fillStyle="white";
    canvas.style.cursor="url(eraser.png),auto";
}

function click_pen(){
    type=0;
    CC();
    canvas.style.cursor="url(pen.png),auto";
}

function click_clear(){
    context.clearRect(0, 0, canvas.width, canvas.height);
    Push();
}

function click_font()
{
    type=2;
    CC();
    canvas.style.cursor="url(T.png),auto"
}

function click_square()
{
    type=3;
    CC();
    canvas.style.cursor="url(square.png),auto"
}

function click_circle()
{
    type=4;
    CC();
    canvas.style.cursor="url(circle.png),auto"
}

function click_triangle()
{
    type=5;
    CC();
    canvas.style.cursor="url(triangle.png),auto"
}


//text
var font_style=document.getElementById('font_style');
var font_text=document.getElementById('font_text');
var font_size=document.getElementById('font_size');
var font_x=document.getElementById('font_x');
var font_y=document.getElementById('font_y');

function CFS(e)
{
    var pos = getMousePos(canvas,e);
    var str=font_size.value+"px "+font_style.value;
    context.font = str;
    var text=font_text.value;
    context.fillText(text,pos.x,pos.y);
}


//redo & undo

var undo = document.getElementById('undo');
var redo = document.getElementById('redo');


undo.addEventListener('click',Undo);
redo.addEventListener('click',Redo);

var cPushArray = new Array();
var cStep = -1;


function Push()
{
    cStep++;
    if(cStep < cPushArray.length)
    {
        cPushArray.length = cStep;
    }
    //cPushArray.push(canvas.toDataURL());
    cPushArray[cStep]=canvas.toDataURL();
}


function Undo()
{
    if(cStep > 0)
    {
        cStep--;
        var canvasPic = new Image();
        canvasPic.src = cPushArray[cStep];
        canvasPic.onload = function(){
            context.drawImage(canvasPic,0,0,canvas.width, canvas.height);
        }
    }
}

function Redo()
{
    if(cStep < cPushArray.length)
    {

        cStep++;
        var canvasPic = new Image();
        canvasPic.src = cPushArray[cStep];
        canvasPic.onload = function()
        {
            context.drawImage(canvasPic,0,0,canvas.width, canvas.height);
        }
    }
}


//upload

var upload = document.getElementById('upload');
upload.addEventListener('change',paste,false);

function paste(e)
{
    var loader = new FileReader();
    loader.onload = function(event){
        var img = new Image();
        img.onload = function(){
            canvas.width = img.width;
            canvas.height = img.height;
            context.drawImage(img,0,0);
            context.lineWidth = radius*2;
        }
        img.src = event.target.result;
    }
    loader.readAsDataURL(e.target.files[0]);
}

//download
function downloadCanvas(link ,filename){
    link.href = canvas.toDataURL();
    link.download = filename;
}

var save=document.getElementById('save');
save.addEventListener('click',function(){
    downloadCanvas(this, Math.random().toString(36).substr(2,9));
},false)











//begining

Push();
click_pen();




//undobutton.addEventListener('click', Undo);
//redobutton.addEventListener('click', Redo);


/*
var undobutton = document.getElementById('left');
var redobutton = document.getElementById('redo');

undobutton.addEventListener('click', undo);
redobutton.addEventListener('click', redo);

function push(){
    steps++;
    if (steps < array.length){ 
        array.length = steps; 
    }
    array.push(canvas.toDataURL());
}

function undo(){
    if (steps > 0) {
        steps--;
        var canvasPic = new Image();
        canvasPic.src = array[steps];
        canvasPic.onload = function(){ 
            context.drawImage(canvasPic, 0, 0, canvas.width, canvas.height);
        }
    }
}

function redo(){
    if (steps < array.length) {
        steps++;
        var canvasPic = new Image();
        canvasPic.src = array[steps];
        canvasPic.onload = function(){ 
            context.drawImage(canvasPic, 0, 0, canvas.width, canvas.height); 
        }
    }
}
*/
